#include <iostream>

#include <string>
#include <memory>
#include "HttpServer.hpp"
#include "TcpServer.hpp"

int main(int argc, char *argv[])
{
    if (argc != 2)
    {
        cout << "Usage Error , Please ：" << argv[0] << " + port!" << endl;
        exit(USAGE);
    }
    int port = atoi(argv[1]);
    std::shared_ptr<HttpServer> http_server(new HttpServer(port));

    http_server->InitServer();
    http_server->Loop();
    return 0;
}