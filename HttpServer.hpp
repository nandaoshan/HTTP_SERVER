#pragma once

#include <iostream>
#include <signal.h>

#include <cstdio>
#include "Task.hpp"
#include "ThreadPool.hpp"
#include "Log.hpp"
#include "TcpServer.hpp"

#define PORT 8080

class HttpServer
{
private:
    int port;
    bool stop;

public:
    HttpServer(int _port = PORT) : port(_port), stop(false)
    {
    }
    void InitServer()
    {
        // SIGPIPE信号需要忽略，否则写入时可能直接终端崩溃server;
        signal(SIGPIPE, SIG_IGN);
    }
    void Loop()
    {
        TcpServer *tsvr = TcpServer::getinstance(port); // sock bind listen里面就处理了
        LOG(INFO, "Loop Begin");

        while (!stop)
        {

            sockaddr_in peer;
            socklen_t len = sizeof(peer);

            int sock = accept(tsvr->Sock(), (sockaddr *)&peer, &len);
            if (sock < 0)
                continue;

            LOG(INFO, "Get a new link"); //到这里 httpserver整体就能接收新连接了！

            // pthread_t tid;
            // int *psock = new int(sock);
            // pthread_create(&tid, nullptr, CallBack::HandlerRequest, psock);
            // pthread_detach(tid);

            Task task(sock);
            Thread_Pool::getinstance()->PushTask(task);
        }
    }
    ~HttpServer() {}
};