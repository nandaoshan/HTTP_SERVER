#pragma once

#include <iostream>
#include <unistd.h>
#include <unordered_map>
#include <sys/types.h>
#include <wait.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <string>
#include <vector>
#include <sys/types.h>
#include <sys/socket.h>
#include <algorithm>
#include <sstream>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/sendfile.h>
#include "Util.hpp"
#include "Log.hpp"
using std::cerr;
using std::cout;
using std::endl;
using std::string;
using std::stringstream;
using std::unordered_map;
using std::vector;
#define OK 200
#define NOT_FOUND 404
#define BAD_REQUEST 400
#define SERVER_ERROR 500

#define HTTP_VERSION "HTTP/1.0"
#define LINE_END "\r\n" //相应出去按\r\n  读的时候是\n
#define WEB_ROOT "wwwroot"
#define HOME_PAGE "index.html"
#define PAGE_404 "404.html"

void f(string s)
{
    cout << endl
         << s << endl;
}
//后缀->Content-Type
static std::string Suffix2Desc(const std::string &suffix)
{
    static std::unordered_map<std::string, std::string> suffix2desc = {
        {".html", "text/html"},
        {".css", "text/css"},
        {".js", "application/javascript"},
        {".jpg", "application/x-jpg"},
        {".xml", "application/xml"},
    };

    auto iter = suffix2desc.find(suffix);
    if (iter != suffix2desc.end())
    {
        return iter->second;
    }
    return "text/html"; //默认返回html的type
}
//状态码->状态描述
static string Code2Desc(int code)
{
    std::string desc;
    switch (code)
    {
    case 200:
        desc = "OK";
        break;
    case 404:
        desc = "Not Found";
        break;
    default:
        break;
    }
    return desc;
}
//请求类
class HttpRequest
{
public:
    string request_line;           //读取请求行
    vector<string> request_header; //读取请求报头
    string blank;                  //空行分隔符
    string request_body;           //请求报文主体(可能没有)

    //解析完毕之后的结果

    //解析请求行三部分
    string method;
    string uri; // path?args
    string version;

    //解析请求报头
    unordered_map<string, string> header_kv;
    int content_length; //请求body的大小
    string path;        //请求路径
    string suffix;      //后缀 .html  <-> query_string: type/html
    string query_string;

    bool cgi; // cgi技术开关
    int size;

public:
    HttpRequest() : content_length(0), cgi(false) {}
    ~HttpRequest() {}
};

//响应类
class HttpResponse
{
public:
    string status_line;             //状态行
    vector<string> response_header; //响应报头
    string blank;                   //空行分隔符
    string response_body;           //响应报文主体(html)

    int status_code; //状态码
    int fd;          //打开的response文件fd
    int size;        // body size

public:
    HttpResponse() : blank(LINE_END), status_code(OK), fd(-1) {}
    ~HttpResponse() {}
};

//读取请求，分析请求，构建响应
// IO通信
class EndPoint
{
private:
    int sock;
    HttpRequest http_request;
    HttpResponse http_response;
    bool stop; //处理读取错误

private:
    bool RecvHttpRequestLine() //读取请求行
    {
        auto &line = http_request.request_line;
        if (Util::ReadLine(sock, line) <= 0)
        {
            stop = true;
        }
        else
        {
            line.resize(line.size() - 1); //去掉多余的'\n',塞入日志; 这个risize妙！
            LOG(INFO, http_request.request_line);
        }

        return stop;
    }
    bool RecvHttpRequestHeader() //读取请求报头 去掉多余的\n
    {

        auto &v = http_request.request_header;
        while (1) //注意 vector[0]没有值的时候只能push_back进去噢  v[0]=？ 会段错误 越界;
        {
            string line;
            if (Util::ReadLine(sock, line) <= 0)
            {
                stop = true;
                break;
            }

            if (line == "\n")
            {
                http_request.blank = line; //空行
                break;
            }
            //正常 k:v \n

            line.resize(line.size() - 1); //去\n
            http_request.request_header.push_back(line);
            // LOG(INFO, line);
        }
        return stop;
    }

    bool IsNeedRecvHttpRequestBody() //判断需不需要读 POST方法+存在contentlength，就要读取body了
    {
        auto &method = http_request.method;
        auto &mp = http_request.header_kv;

        if (method == "POST")
        {
            if (mp.find("Content-Length") != mp.end())
            {

                http_request.content_length = atoi(mp["Content-Length"].c_str()); //记录一下body的size

                return true;
            }

            return false;
        }
        return false;
    }
    bool RecvHttpRequestBody()
    {
        if (IsNeedRecvHttpRequestBody())
        {

            int len = http_request.content_length; //这里不能&，不然下面循环 原来的size就减没了，为啥这么精确 -->防止粘包
            auto &body = http_request.request_body;
            for (int i = 0; i < len; i++)
            {
                char c;
                int s = recv(sock, &c, 1, 0); //流式读取
                if (s > 0)
                {
                    body += c;
                }
                else
                {
                    stop = true;
                    break;
                }
            }
            // cout << endl;
            // cout << body << endl;

            return stop;
        }
    }
    void ParseHttpRequestLine() //解析请求行，入method，uri，version
    {

        // GET / HTTP/1.1 三部分用" "分隔
        stringstream ss(http_request.request_line);
        // cout<<http_request.request_line<<endl;
        ss >> http_request.method >> http_request.uri >> http_request.version;

        auto &method = http_request.method;
        std::transform(method.begin(), method.end(), method.begin(), ::toupper); //将请求方法大小写同一;

        // cout<<http_request.method<<http_request.uri<<http_request.version<<endl;
    }
    void ParseHttpRequestHeader() //解析请求报头,入header_kv;
    {
        auto &mp = http_request.header_kv;
        auto &v = http_request.request_header;
        for (auto &e : v)
        {
            //"k:v"->mp(k,v)
            string k, v;
            Util::CutString(e, k, v, ": "); //"： "注意这个细节，不然分不到位
            mp[k] = v;
        }
        // for(auto&e:mp){
        //     cout<<e.first<<":"<<e.second<<endl;
        // }
    }
    int ProcessNonCgi() //简单的静态网页返回，我们只需要打开fd就行了，跟后续的错误处理404页面构建部份复用
    {

        http_response.fd = open(http_request.path.c_str(), O_RDONLY);
        if (http_response.fd >= 0)
        {

            return OK;
        }
        return 404;
    }
    int ProcessCgi()
    {
        int code = OK;
        auto &bin = http_request.path;                     // cgi.exe的位置,子进程exec它
        auto &method = http_request.method;                // GET OR POST
        auto &body = http_request.request_body;            // POST 多 直接write到child
        auto &querystring = http_request.query_string;     // GET 少 利用环境变量
        int content_length = http_request.content_length;  // boylength  -> 环境变量 ->cgi知道读多少个
        auto &response_body = http_response.response_body; // cgi的响应正文

        string query_string_env;
        string method_env;
        string content_length_env;
        //站在父进程的角度创建匿名管道;
        int input[2];
        int output[2];
        if (pipe(input) < 0)
        {
            LOG(ERROR, "pipe input error!");
            code = SERVER_ERROR;
            return code;
        }
        if (pipe(output) < 0)
        {
            LOG(ERROR, "pipe output error!");
            code = SERVER_ERROR;
            return code;
        }

        //创建子进程，进行cgi
        pid_t pid = fork();
        if (pid == 0)
        { // child
            close(input[0]);
            close(output[1]);
            //在子进程角度
            // input[0]:读入->fd:0<->output[0];
            // input[1]:写出->fd:1<->input[1];
            dup2(output[0], 0);
            dup2(input[1], 1);
            //让替换的cgi程序知道GET还是POST方法，对应选择接收数据的方式
            method_env = "METHOD=";
            method_env += method;
            putenv((char *)method_env.c_str());

            if (method == "GET")
            {
                query_string_env = "QUERY_STRING=";
                query_string_env += querystring;
                putenv((char *)query_string_env.c_str());
            }
            else if (method == "POST")
            {
                content_length_env = "CONTENT_LENGTH=";
                content_length_env += std::to_string(content_length);
                putenv((char *)content_length_env.c_str());
            }
            else
            {
                // DO Nonthing
            }

            // exec* bin
            // dup2替换fd之后，execl替换程序直接对0，1进行读取与写入，实际上就是与http_server的读取和写入

            execl(bin.c_str(), bin.c_str(), nullptr);

            exit(1); // execl失败
        }
        else if (pid < 0)
        { // error;
            return 404;
            LOG(ERROR, "fork error!");
        }
        else
        {                     // parent
            close(input[1]);  //父从cgi读，关掉写
            close(output[0]); //夫给cgi写，关掉读

            if (method == "POST")
            {
                const char *start = body.c_str();
                int total = 0;
                int size = 0;

                while (total < content_length && (size = write(output[1], start + total, body.size() - total)) > 0) // bug
                {
                    total += size;
                }
            }

            // cgi程序cout返回给server，下面开始读取！
            char c;
            while (read(input[0], &c, 1) > 0)
            {
                response_body += c; //响应报文
            }
            int status = 0;
            pid_t ret = waitpid(pid, &status, 0);
            if (ret == pid)
            { //等待有可能失败,比如cgiexe出错崩溃了，得再做判断;
                if (WIFEXITED(status))
                {
                    if (WEXITSTATUS(status) == 0)
                    {
                        code = OK;
                    }
                    else
                    { //正常退出，结果不正确
                        code = SERVER_ERROR;
                    }
                }
                else
                { //不正确退出
                    code = SERVER_ERROR;
                }
            }
            //资源释放
            close(input[0]);
            close(output[1]);
        }

        return code;
    }
    void HandlerError(string page)
    {
        http_request.cgi = false; //只要出错，我们就cgi = false，最后send正常的静态错误网页
        //返回404.html

        //打开静态错误网页，send的时候直接sendfile发回去了;
        http_response.fd = open(page.c_str(), O_RDONLY);
        if (http_response.fd > 0)
        {
            struct stat st;
            stat(page.c_str(), &st);
            string line = "Cntent-Type: text/html";
            line += LINE_END;
            http_response.response_header.push_back(line);

            line = "Cntent-Length: ";
            line += std::to_string(st.st_size);
            line += LINE_END;
            http_response.response_header.push_back(line);
            http_response.size = st.st_size;
        }
    }
    void BuildOkResponse()
    {
        string line = "Cntent-Type: ";
        line += Suffix2Desc(http_request.suffix);
        line += LINE_END;
        http_response.response_header.push_back(line);

        line = "Content-Length: ";
        if (http_request.cgi)
        {
            line += std::to_string(http_response.response_body.size()); // cgi程序 返回body
        }
        else
        {
            line += std::to_string(http_response.size); // Noncgi 静态网页
        }
        line += LINE_END;
        http_response.response_header.push_back(line);
    }
    void BuildHttpResponseHelper()
    {
        auto &status_code = http_response.status_code;
        //构建状态行
        auto &status_line = http_response.status_line;
        status_line += HTTP_VERSION;
        status_line += " ";
        status_line += std::to_string(status_code);
        status_line += " ";
        status_line += Code2Desc(status_code);
        status_line += LINE_END;

        string path = WEB_ROOT;

        //构建响应正文，可能包括header
        switch (status_code)
        {
        case OK:
            BuildOkResponse();
            break;

        case NOT_FOUND:
            path += '/';
            path += PAGE_404;
            HandlerError(path);
            break;

        case BAD_REQUEST:
            path += '/';
            path += PAGE_404;
            HandlerError(path);
            break;
        case SERVER_ERROR:
            path += '/';
            path += PAGE_404;
            HandlerError(path);
            break;
        default:
            break;
        }
    }

public:
    EndPoint(int _sock) : sock(_sock), stop(false)
    {
    }
    bool IsStop()
    {
        return stop;
    }
    void RecvHttpRquest()
    {

        // Recv 都成功 没退出 再Parse
        if (RecvHttpRequestLine() || RecvHttpRequestHeader())
        {
            // recv error
        }
        else
        {
            // Parse
            ParseHttpRequestLine();
            ParseHttpRequestHeader();
            RecvHttpRequestBody(); // Parse完line和header 才能recvbody
        }
    }

    void BuildHttpResponse()
    {
        //请求全部读完了，构建响应
        struct stat st;

        ssize_t rfound;
        string _path; // temp
        auto &status_code = http_response.status_code;
        auto &method = http_request.method;
        if (method != "GET" && method != "POST")
        {
            //非法method

            status_code = BAD_REQUEST;
            LOG(WARNING, "method error!");
            goto END;
        }

        //构建请求路径path 和 请求文件大小size;
        if (method == "GET")
        {
            if (http_request.uri.find("?") != string::npos)                                           // get 带参// 引入cgi
            {                                                                                         // GET:  path? content=...参数
                Util::CutString(http_request.uri, http_request.path, http_request.query_string, "?"); //构建path路径
                http_request.cgi = true;                                                              //有参数 引入cgi
            }
            else
            {
                http_request.path = http_request.uri;
            }
        }
        else if (method == "POST") // cgi
        {
            http_request.path = http_request.uri;
            http_request.cgi = true;
        }
        else
        {
            // DO Noting
        }

        //请求路径 我们上层得套wwwroot，index.html等默认
        _path = http_request.path;
        http_request.path = WEB_ROOT;
        http_request.path += _path;

        //如果路径末尾为'/' 意味着是个目录，我们需要套上index.html
        if (http_request.path.find('/') == http_request.path.size() - 1)
        {
            http_request.path += HOME_PAGE;
        }

        //判断文件存在？存在属性保存进st

        if (stat(http_request.path.c_str(), &st) == 0)
        {
            if (S_ISDIR(st.st_mode))
            {
                //是个目录不是html文件，特殊处理到默认
                http_request.path += '/';
                http_request.path += HOME_PAGE;
                stat(http_request.path.c_str(), &st); //更新path文件的信息
            }
            if ((st.st_mode & S_IXUSR) || (st.st_mode & S_IXGRP) || (st.st_mode & S_IXOTH))
            {
                //是个可执行程序！不是html
                http_request.cgi = true; //特殊处理cgi
            }

            http_response.size = st.st_size;
        }
        else
        { //说明资源是不存在的
            LOG(WARNING, http_request.path + " Not Found!");
            status_code = NOT_FOUND;
            goto END;
        }
        //构建suffix后缀
        rfound = http_request.path.rfind("."); //构建suffix:<-->type映射;
        if (rfound == string::npos)
        { //没有.后缀 //suffix 默认 .html
            http_request.suffix = ".html";
        }
        else
        {
            http_request.suffix = http_request.path.substr(rfound); //.xxx 文件类型
        }

        // cgi处理还是Noncgi处理？
        if (http_request.cgi)
        {
            status_code = ProcessCgi(); //执行目标程序，拿到结果:http_response.response_body;
        }
        else
        {
            // 1. 目标网页一定是存在的
            // 2. 返回并不是单单返回网页，而是要构建HTTP响应！全套!

            status_code = ProcessNonCgi(); //简单的网页返回，返回静态网页,只需要打开即可
        }

    END:
        //构建响应
        BuildHttpResponseHelper(); //状态行填充了，响应报头也有了， 空行也有了，正文有了
    }

    void SendHttpResponset()
    {

        //发状态行
        send(sock, http_response.status_line.c_str(), http_response.status_line.size(), 0);
        //发报头

        for (auto iter : http_response.response_header)
        {
            send(sock, iter.c_str(), iter.size(), 0);
        }
        //发\n
        send(sock, "\n", 1, 0);
        //发body

        if (http_request.cgi)
        {
            auto &response_body = http_response.response_body;
            int size = 0;
            int total = 0;
            const char *start = response_body.c_str();

            while (total < response_body.size() && (size = send(sock, start + total, response_body.size() - total, 0)) > 0)
            {
                total += size;
            }
        }
        else
        {
            sendfile(sock, http_response.fd, nullptr, http_response.size);
        }

        close(http_response.fd);
    }
    ~EndPoint()
    {
        close(sock);
    }
};

//#define DEBUG //测试
class CallBack
{
public:
    CallBack() {}
    void operator()(int sock)
    {
        HandlerRequest(sock);
    }

    void HandlerRequest(int sock)
    {

        LOG(INFO, "========================HandlerRequest Begin==================================");
#ifdef DEBUG
        // for test
        char buff[4022];
        int s = recv(sock, buff, 4022, 0);
        buff[s - 1] = '\0';
        cout << "===============begin===============" << endl;
        cout << buff << endl;
        cout << "===============end===============" << endl;
#else
        EndPoint *ep = new EndPoint(sock);
        string line;
        ep->RecvHttpRquest();
        if (!ep->IsStop())
        {
            LOG(INFO, "Recv No Error,Build and Send!");
            ep->BuildHttpResponse();
            ep->SendHttpResponset();
        }
        else
        {
            LOG(WARNING, "Recv Error!Stop Build and Send!");
        }

        delete ep;

        return;
#endif
        LOG(INFO, "========================HandlerRequest End==================================");
    }
    ~CallBack() {}
};