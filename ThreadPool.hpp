#pragma once
#include "Task.hpp"
#include <iostream>
#include <pthread.h>
#include <queue>
#include "Log.hpp"
using std::queue;

#define NUM 6

class Thread_Pool
{
private:
    int num;
    queue<Task> task_queue;
    bool stop;
    pthread_mutex_t lock;
    pthread_cond_t cond;

    static Thread_Pool *single_instance;
    Thread_Pool(int _num = NUM) : num(_num), stop(false)
    {
        pthread_mutex_init(&lock, nullptr);
        pthread_cond_init(&cond, nullptr);
    }
    Thread_Pool(const Thread_Pool &) {}

public:
    static Thread_Pool *getinstance() //单例
    {
        static pthread_mutex_t _mutex = PTHREAD_MUTEX_INITIALIZER;
        if (single_instance == nullptr)
        {
            pthread_mutex_lock(&_mutex);
            if (single_instance == nullptr)
            {
                single_instance = new Thread_Pool();
                single_instance->InitThreadPool();
            }
            pthread_mutex_unlock(&_mutex);
        }
        return single_instance;
    }

    bool TaskQueueIsEmpty()
    {
        return task_queue.size() == 0 ? true : false;
    }
    void Lock()
    {
        pthread_mutex_lock(&lock);
    }

    void Unlock()
    {
        pthread_mutex_unlock(&lock);
    }
    bool IsStop()
    {
        return stop;
    }
    void ThreadWait()
    {
        pthread_cond_wait(&cond, &lock);
    }
    void ThreadWakeup()
    {
        pthread_cond_signal(&cond);
    }
    static void *ThreadTRoutine(void *args)
    {
        Thread_Pool *tp = (Thread_Pool *)args;

        while (true)
        {
            Task t;
            tp->Lock();
            while (tp->TaskQueueIsEmpty())
            {
                tp->ThreadWait(); //当我醒来一定占有互斥锁!
            }
            tp->PopTask(t);
            tp->Unlock();

            t.ProcessOn(); // CallBack回调处理，处理这个sock链接
        }
    }
    bool InitThreadPool()
    {
        for (int i = 0; i < num; i++)
        {
            pthread_t tid;
            if (pthread_create(&tid, nullptr, ThreadTRoutine, this) != 0)
            {
                LOG(FATAL, "create thread pool error");
            }
        }
        LOG(INFO, "create thread pool success");
        return true;
    }

    void PushTask(const Task &task)
    {
        Lock();
        task_queue.push(task);
        Unlock();
        ThreadWakeup();
    }
    void PopTask(Task &task)
    {
        //上层调用pop加过锁了
        task = task_queue.front();
        task_queue.pop();
    }
    ~Thread_Pool()
    {
        pthread_mutex_destroy(&lock);
        pthread_cond_destroy(&cond);
    }
};

Thread_Pool *Thread_Pool::single_instance = nullptr;