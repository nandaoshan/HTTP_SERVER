#pragma once

#include <iostream>
#include <string>
#include <cstring>
#include <sys/types.h>
#include <sys/socket.h>
using std::string;
//工具类
class Util
{
public:
    static int ReadLine(int sock, string &out) //按一行读取报文,返回长度;
    {
        char c = 'X';
        while (c != '\n')
        {
            ssize_t s = recv(sock, &c, 1, 0); //(注意，有的报文以\r\n 或者 \r结尾，统一处理为\n,同时考虑数据粘包问题进行读取！)
            if (s > 0)
            {
                if (c == '\r')
                {
                    recv(sock, &c, 1, MSG_PEEK); //窥探一下
                    if (c == '\n')
                    { //窥探成功！大胆拿走这个\n 放入c中
                        recv(sock, &c, 1, 0);
                    }
                    else
                    { //窥探失败，直接换掉这个\r
                        c = '\n';
                    }
                }
                out += c;
            }
            else if (s == 0)
            {
                return 0;
            }
            else
            { //出错
                return -1;
            }
        }

        return out.size();
    }

    static bool CutString(const std::string &target, std::string &sub1_out, std::string &sub2_out, std::string sep)
    {
        size_t pos = target.find(sep);
        if (pos != string::npos)
        {

            sub1_out = target.substr(0, pos);
            sub2_out = target.substr(pos + sep.size()); //": "header以这个分割的,那就得+2！，注意细节，正常的"?"来分割就加1，实现了通用！！
            return true;
        }
        return false;
    }
};