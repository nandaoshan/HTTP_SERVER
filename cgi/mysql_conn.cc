#include "comm.hpp"
#include "mysql.h"

bool InsertSql(string sql)
{
    MYSQL *conn = mysql_init(nullptr);     //创建mysql句柄
    mysql_set_character_set(conn, "utf8"); //程序和mysql通信的时候 采用utf-8 防止乱码
    //链接mysql
    if (nullptr == mysql_real_connect(conn, "127.0.0.1", "http_test", "12345678", "http_test", 3306, nullptr, 0))
    {
        cerr << "connect mysql error!" << endl;
        return 1;
    }

    cerr << "connect mysql success!" << endl;

    cerr << "query : " << sql << endl;
    int ret = mysql_query(conn, sql.c_str()); //向mysql下发命令
    cerr << "ret : " << ret << endl;

    mysql_close(conn);
    return true;
}
int main()
{
    string query_string;

    if (GetQueryString(query_string)) //从HTTP_SERVER获取参数
    {
        cerr << "query_string : " << query_string.c_str() << endl;
        //参数处理;类似于test_cgi的处理数据逻辑;

        // name=xxx&passwd=xxx
        string name;
        string passwd;
        CutString(query_string, "&", name, passwd);

        //参数进一步拆分
        string _name;
        string sql_name;
        CutString(name, "=", _name, sql_name);
        string _passwd;
        string sql_passwd;
        CutString(passwd, "=", _passwd, sql_passwd);

        string sql = "insert into user(name,passwd) values(\'";
        sql += (sql_name + "\',");
        sql += (sql_passwd + ")");

        // sql语句构建号以后，插入数据库; 返回一个简单地提示网页！
        if (InsertSql(sql))
        {
            cout << "<html>";
            cout << "<head><meta charset=\"utf-8\"></head>";
            cout << "<body><h1>注册成功！信息已经插入后台数据库!</h1></body>";
        }
    }

    return 0;
}
