#include "comm.hpp"

int main()
{
    cerr << "========================cgi begin===================" << endl; //用cerr测试，亦谓cout已经被我们替换成管道了！
    string query_string;
    GetQueryString(query_string);
    // a=100&b=200
    // a,100,b,200

    //数据分析
    string str1, str2;

    string name1, value1;
    string name2, value2;
    CutString(query_string, "&", str1, str2);
    CutString(str1, "=", name1, value1);
    CutString(str2, "=", name2, value2);

    int x = atoi(value1.c_str());
    int y = atoi(value2.c_str());
    //可能向进行某种计算(计算，搜索，登陆等)，想进行某种存储(注册)
    cout << "<html>";
    cout << "<head><meta charset=\"utf-8\"></head>";
    cout << "<body>";
    //往fd1输出，到httpserver了
    cout << name1 << " : " << value1 << endl;

    cout << name2 << " : " << value2 << endl;
    cout << "<h3> " << value1 << " + " << value2 << " = " << x + y << "</h3>";
    cout << "<h3> " << value1 << " - " << value2 << " = " << x - y << "</h3>";
    cout << "<h3> " << value1 << " * " << value2 << " = " << x * y << "</h3>";
    cout << "<h3> " << value1 << " / " << value2 << " = " << x / y << "</h3>";
    //假设/0错误，cgi崩溃，父进程wait到的车状态就会异常,直接就错误处理返回静态错误网页了 不需要担心;
    cout << "</body>";
    cout << "</html>";

    cerr << "========================cgi end===================" << endl;
    return 0;
}