#pragma once

#include <iostream>
#include <cstdlib>
#include <unistd.h>

using namespace std;
bool GetQueryString(string &query_string)
{
    bool result = false;
    string method = getenv("METHOD");
    cerr << "METHOD = " << method << endl;

    if (method == "GET")
    {

        query_string = getenv("QUERY_STRING");

        result = true;
    }
    else if (method == "POST")
    {
        cerr << "Content-length = " << getenv("CONTENT_LENGTH") << endl;
        int count_length = atoi(getenv("CONTENT_LENGTH"));

        while (count_length--)
        {
            char c;

            read(0, &c, 1);
            query_string += c;
        }

        result = true;
    }
    else
    {
        result = false;
    }
    return result;
}
void CutString(string &in, const string &sep, string &out1, string &out2)
{
    int index;
    if ((index = in.find(sep)) != string::npos)
    {
        out1 = in.substr(0, index);
        out2 = in.substr(index + sep.size());
    }
}