#pragma once

#include <iostream>
#include <cstdlib>
#include <cstring>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <pthread.h>
#include "Log.hpp"
using std::cout;
using std::endl;
#define BACKLOG 5
enum ERR
{
    SOCK_ERR = 1,
    BIND_ERR,
    LISTEN_ERR,
    USAGE
};
class TcpServer
{
private:
    int port;
    int listen_sock;
    static TcpServer *svr;

private:                               //单例模式
    TcpServer(int _port) : port(_port) //构造
    {
    }
    TcpServer(const TcpServer &s) //拷贝
    {
    }

public:
    static TcpServer *getinstance(int port) //单例模式
    {
        static pthread_mutex_t lock = PTHREAD_MUTEX_INITIALIZER;
        if (nullptr == svr)
        {
            pthread_mutex_lock(&lock);
            if (nullptr == svr)
            {
                svr = new TcpServer(port);
                svr->InitServer(); // getinstance的时候就搞定了sock bind listen了;
            }
            pthread_mutex_unlock(&lock);
        }
        return svr;
    }

public:
    void InitServer()
    {
        Socket();
        Bind();
        Listen();

        LOG(INFO, "TcpServer begin");
    }
    void Socket()
    {
        listen_sock = socket(AF_INET, SOCK_STREAM, 0);
        if (listen_sock < 0)
        {
            LOG(FATAL, "socket error");
            exit(SOCK_ERR);
        }
        //防止bind error
        int opt = 1;
        setsockopt(listen_sock, SOL_SOCKET, SO_REUSEADDR, &opt, sizeof(opt));
    }
    void Bind()
    {
        sockaddr_in local;
        bzero(&local, sizeof(local));
        local.sin_family = AF_INET;
        local.sin_port = htons(port);
        local.sin_addr.s_addr = INADDR_ANY; //云服务器这样绑

        if (bind(listen_sock, (sockaddr *)&local, sizeof(local)) < 0)
        {
            LOG(FATAL, "bind error");
            exit(BIND_ERR);
        }
    }
    void Listen()
    {
        if (listen(listen_sock, BACKLOG) < 0)
        {
            LOG(FATAL, "listen error");
            exit(LISTEN_ERR);
        }
    }
    int Sock()
    {
        return listen_sock;
    }
    ~TcpServer()
    {
        if (listen_sock > 0)
            close(listen_sock);
    }
};
//单例
TcpServer *TcpServer::svr = nullptr;