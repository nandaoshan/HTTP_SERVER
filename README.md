# 项目详细介绍博客

[博客链接](https://blog.csdn.net/wtl666_6/article/details/127472921)

# 项目背景

> http协议被广泛使用，从移动端，pc端浏览器，http协议无疑是打开互联网应用窗口的重要协议，http在网络应用层中的地位不可撼动，是能准确区分前后台的重要协议。

# 项目目标

> 在对[http协议](https://blog.csdn.net/wtl666_6/article/details/126876050?spm=1001.2014.3001.5501)的理论学习的基础上，从零开始完成web服务器开发，坐拥下三层协议，从技术到应用，让网络难点无处遁形。

# 项目描述

> 基于C/B模式，从零构建一个Web服务器，用于处理浏览器发来的请求以及对其响应;编写支持中小型应用的http，并结合mysql，理解常见互联网应用行为，做完该项目，你可以从技术上 完全理解从你上网开始，到关闭浏览器的所有操作中的技术细节!

# 技术要点

- 网络编程（TCP/IP协议, socket流式套接字，http协议）
- Cgi技术
- 多线程技术
- 数据库技术
- 管道重定向
- 进程间通信

# 项目定位

研发岗

- 开发环境 centos 7 ＋ vim/gcc/gdb + C/C++ + VSCode;

